#pragma config(Sensor, in1,    armPotL,        sensorPotentiometer)
#pragma config(Sensor, in2,    armPotR,        sensorPotentiometer)
#pragma config(Sensor, in4,    powerExBattLevel, sensorAnalog)
#pragma config(Sensor, in5,    autonPot,       sensorPotentiometer)
#pragma config(Sensor, in6,    lEye,           sensorLineFollower)
#pragma config(Sensor, in7,    mEye,           sensorLineFollower)
#pragma config(Sensor, in8,    rEye,           sensorLineFollower)
#pragma config(Sensor, dgtl1,  quadR,          sensorQuadEncoder)
#pragma config(Sensor, dgtl3,  quadL,          sensorQuadEncoder)
#pragma config(Sensor, dgtl5,  autonButton,    sensorTouch)
#pragma config(Sensor, dgtl6,  Solenoid1,      sensorDigitalOut)
#pragma config(Sensor, dgtl7,  Solenoid2,      sensorDigitalOut)
#pragma config(Sensor, dgtl11, limitR,         sensorTouch)
#pragma config(Sensor, dgtl12, limitL,         sensorTouch)
#pragma config(Sensor, I2C_1,  rightArmEncoder, sensorNone)
#pragma config(Sensor, I2C_2,  leftArmEncoder, sensorNone)
#pragma config(Motor,  port1,           leftBackMotor, tmotorVex393, openLoop)
#pragma config(Motor,  port2,           intakeL,       tmotorVex393, openLoop)
#pragma config(Motor,  port3,           intakeR,       tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port4,           leftFrontMotor, tmotorVex393, openLoop)
#pragma config(Motor,  port5,           rightFrontMotor, tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port6,           armLBack,      tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port7,           armRFront,     tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port8,           armRBack,      tmotorVex393, openLoop)
#pragma config(Motor,  port9,           armLFront,     tmotorVex393, openLoop)
#pragma config(Motor,  port10,          rightBackMotor, tmotorVex393, openLoop, reversed)

#pragma platform(VEX)
#include "Vex_Competition_Includes.c"

#pragma competitionControl(Competition)
#pragma autonomousDuration(15)
#pragma userControlDuration(105)

/*Arm and Intake Positions*/
#define ARM_STASH 3010
#define ARM_LOW 2050
#define ARM_TOP 3015// highest possible value for scissor lift
#define ARM_BAR 2505 //@TODO set this value
#define ARM_HANG 3015

/*Autonomous Defines*/
/*Field Distances*/
#define DIST_START_BALL90 30
#define DIST_START_BALL45 40
#define DIST_START_3RDBALL 84
#define DIST_HALFWAY_STASH 40
#define DIST_STASH_ 80
#define DIST_STASH 60
#define DIST_BUCKY_HANGING 15
#define DIST_START_HANGING_RAND_BALLS 30
#define DIST_ACROSS_FIELD 120
#define TILE_WIDTH 24

/*Autonomous Timings*/
#define KASY_TURNTIME 2000 //KASY, Nickname for Kasyap, our head strategist
#define KASY_QUICK_TURN 2000
#define WAIT 1000
#define ARM_BAR_TIME 4000
#define ARM_STASH_TIME 3700
#define ARM_HANG_TIME 4950 // time to go up to each area
#define ARM_HANG_LIFT_TIME 3000 // time to lift to hanging height
#define STASH_TIME 1000
#define INTAKE_TIME 2000

#define FORWARD_BUMP 660
#define TURN_2BALL 20
#define BUMP_INTAKE_TIME 750

/*Sensor Movement Defines*/
#define gearratio 1.0
#define dia 4.0 //multiplied by 2
#define wheelbase 13.0
#define ARM_KP 1.0 //the ratio that determines how fast arm motors move based on the difference in height
#define driveratio 1.0 //the ratio that determines how fast drive motors move based on difference in distance travelled
#define LINE_VALUE 2700
#define RAMP_UP_RATE 10 //an increased number means it ramps up faster

/*auton select defines*/
#define threeBitFactor 585
#define fourBitFactor 273

#define CAN_GO_BACKWARDS_BUMP 1 //comment this line out if we cannot go backwards over bump

/* Arm Potentiometer Defines */
#define ARM_L_MAX 2955.0
#define ARM_R_MAX 3015.0
#define ARM_L_MIN 1925.0
#define ARM_R_MIN 2050.0

/********************************** V 4.0 "KasyCrustTime" *************************************/
//Sensors:
//-Line Track
//-Encoder R
//-Potentiometer Auton
//-LCD Display
//-Kasy Bumper Button
//
//Motors:
//-Drive (6 Wheel, 4 Motor)
//-Arm (Scissor Design, 4 Motor)
//-Intake (Side Design Temp 2 Motor)
//
//Pneumatics:
//Solenoids 1&2 (Double Acting Pistons)
//
//Timers:
//T1 = LCD message timer
//T2 = Ramp up timer
//
//Updates From V 3.0.5:
//
//Refined 45 point skills
//
/********************************************************************************************/

float powEx = SensorValue[powerExBattLevel];
int button = 0;
float ratio = (ARM_R_MAX - ARM_R_MIN) / (ARM_L_MAX - ARM_L_MIN);
//**LCD Variables**//
//const short leftButton = 1;
const short centerButton = 2; //LCD Buttons
const short rightButton = 4;
int x = 0; //LCD message Variable
int count = 0; //Auton Select
bool locked = false;
int valLift;

//**Motion variables**//
int driveTaskDistance; // the distance to go in the drive task
long prevTime; // the previous time on the T2 100 ms counter (if the current time is different from this, change the motor values)
int motorValues[10]; //the values of the motors

/*LCD Strings*/
string mainBattery;
string powerExpanderBattery;
string backupBattery;
string batteries;
string levels;
string autonNames[20];//display to choose auton
string messages[34];//funny messages for our lcd
char name[16];

/*****************Prototypes***************/
//*****LCD FUNCTIONS*****//
void waitForRelease();
void clearLCD();
void displayChange(int j);
void drawName(int line);
void messageLCD();
void batteryLCD();

//*****MISCELLANEOUS FUNCTIONS*****//
int map(int x, float in_min, float in_max, float out_min, float out_max);
void waitAutonButton();
int sign(int val);
int nonzero(int value);
int fixDeadZone(int value);
void clearDriveEncoders();
void clearArmEncoders();
int mapSensor(int val);
int mapLineFollower(int val);

//*****MOTION FUNCTIONS*****//
void setMotor(tMotor index, int val);
void arm(int val);
void arm(int l, int r);
void move(int l, int r);
void moveRamped(int l, int r);
void move(int lf, int lb,int rf,int rb);
void moveRamped(int lf, int lb, int rf, int rb);
void intake(int val);

//*****PNEUMATICS*****//
void piston1();
void piston2();
void throw();

//*****AUTON FUNCTIONS*****//
void waitUntilLine(int val);
void waitUntilAllLines(int val);
void moveUntilRamped(int l, int r);
void goForward(int inches);
void goBackward(int inches);
void goAsync(int inches);
void hang();
void jerk();
void lineTrace(int inches);
void lineTraceAsync(int inches);
void liftArm(int speed, int armPotValue);
void lowerArm(int speed);
void rturn(int bodydegs);
void lturn(int bodydegs);

//*****AUTONOMOI*****//
void kasySkillsTime();
void ProgrammingSkills();
void ProgrammingSkills_withButton();
void untie_RED();
void untie_BLUE();
void block_BLUE();
void block_RED();
void easyAuton();
void timeAuton();
void stashAuton();
void stashAuton_lineTracer_RED();
void stashAuton_lineTracer_BLUE();
void hanging_SAFE();
void hanging_GET_BUCKY();
void hanging_GET_BUCKY_NOSHOOT();
void suicide();
void hanging_GET_OVER_BUMP();
void knockover_BALLS_RED();
void knockover_BALLS_BLUE();
void hang_RED();
void hang_BLUE();
void everything_HANGING_BLUE_();
void everything_HANGING_RED_();

task runLiftArm();
//task runLowerArm();
/************************LCD Functions**************************/
void waitForRelease()
{
	while(nLCDButtons != 0){}
	wait1Msec(5);
}

void clearLCD(){
	clearLCDLine(0);
	clearLCDLine(1);
}

void displayChange(int j){

	if(j != -1){
		displayLCDCenteredString(1,autonNames[count]);
		}else{
		clearLCD();
		displayLCDCenteredString(0, autonNames[count]);
		displayLCDCenteredString(1, "is Running");
	}

}

void drawName(int line){
	char c = 253;
	string s = c;
	strcat (name,"$$  4");
	strcat (name, s);
	strcat (name, "CHAIN");
	c = 228;
	s = c;
	strcat (name,s);
	strcat (name,"  $$");

	displayLCDCenteredString(line,name);
}

void messageLCD(){
	bLCDBacklight = true;
	clearLCD();

	messages[0]  = "Chisel it out!";
	messages[1]  = "TY Based God";
	messages[2]  = "Black Misery";
	messages[3]  = "Torque Amplifier";
	messages[4]  = "Kemps Nut";
	messages[5]  = "TYBG";
	messages[6]  = "750Crust";
	messages[7]  = "Welch's";
	messages[8]  = "Merry Crustmas!";
	messages[9]  = "Stanley Shi";
	messages[10] = "Modular Chassis";
	messages[11] = "Crust Asians";
	messages[12] = "Bump the Bump";
	messages[13] = "c dis bby rumble";
	messages[14] = "BEES";
	messages[15] = "Kasy Turn Time";
	messages[16] = "SAT Boot Camp";
	messages[17] = "Shmopity Shmop";
	messages[18] = "White Mamba";
	messages[19] = "Crust Bot";
	messages[20] = "EPL :)";
	messages[21] = "Connection Paper";
	messages[22] = "Flipadoo!";
	messages[23] = "Master Control";
	messages[24] = "Goes on for days";
	messages[25] = "Dremel it off!";
	messages[26] = "Great Overall!";
	messages[27] = "Sublime!";
	messages[28] = "Quite Solid";
	messages[29] = "Crustache";
	messages[30] = "Colars!";
	messages[31] = "Git Bash!";
	messages[32] = "Get git!";
	messages[33] = "git commit";

	x = abs(rand()%34);
	displayLCDCenteredString(1,messages[x]);
	displayLCDCenteredString(0,name);
}

void batteryLCD()
{
	bLCDBacklight = true;

	sprintf(mainBattery, "%1.2f%c", nImmediateBatteryLevel/1000.0,'V');
	sprintf(powerExpanderBattery, "%1.2f%c", SensorValue[in6]/280.5, 'V');
	sprintf(backupBattery, "%1.2f%c", BackupBatteryLevel/1000.0, 'V');

	string batteries = "Main  PowEx B-Up";
	snprintf(levels, 17, "%s %s %s", mainBattery, powerExpanderBattery, backupBattery);

	displayLCDString(0, 0, batteries);
	displayLCDString(1, 0, levels);

	wait1Msec(100);
}

/********************Miscellaneous Functions***************************/
int map(int x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void waitAutonButton() {
	while (!SensorValue[autonButton]) wait1Msec(10);
}

int sign(int val) {
	return val / nonzero(abs(val));
}

int nonzero(int value) {
	if (value == 0) return 1; else return value;
}

int fixDeadZone(int value) {
	if (abs(value) < 30)
		return 0;
	else
		return value;
}

void clearDriveEncoders() {
	SensorValue[quadR]=0;
	SensorValue[quadL]=0;
}

void clearArmEncoders() {
	nMotorEncoder[leftArmEncoder] = 0;
	nMotorEncoder[rightArmEncoder] = 0;
}

void clearMotorArrayValues() {
  for (int ind = 0; ind < 10; ind++) {
  	motorValues[ind] = 0;
  }
}

int mapSensor(int val){
	return (int)(((long)(4095 - val)*127)/4095);
}

int mapLineFollower(int val) {
	int inMax = 3000;
	int inMin = 2000;
	int outMin = 0;
	int outMax = 127;
	int newVal = inMax - val;
	long temp = newVal;
	temp *= (long)(outMax - outMin);
	temp /= (long)(inMax - inMin);
	int intTemp = (int)temp + outMin;
	return intTemp;
}

/********************Motion Functions*******************************/
void setMotor(tMotor index, int val)
{
	if ((time1[T2] - prevTime) % 20 == 0) {
		prevTime = time1[T2];

		if (abs(motorValues[index] - val) < 30 || fixDeadZone(val) == 0)	motorValues[index] = val;
		else motorValues[index] = motorValues[index] + RAMP_UP_RATE * sign(val - motorValues[index]);

		if(motorValues[index] == 0) motorValues[index] = 30 * sign(val);
		motor[index] = fixDeadZone(motorValues[index]);
	}
	displayLCDNumber(0, 0, time10[T2]);
}

void arm(int val)
{
	//ONLY TRUE WHEN POSITIVE POT VALUE IS UP (if positive is down, flip all inequalities)
//	int lspeed = fixDeadZone(((val < 0) ? (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) < SensorValue[armPotR]) : (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) > SensorValue[armPotR])) ? val / (ARM_KP * abs(SensorValue[armPotR] - map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX))) : val);
//	int rspeed = fixDeadZone(((val < 0) ? (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) > SensorValue[armPotR]) : (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) < SensorValue[armPotR])) ? val / (ARM_KP * abs(SensorValue[armPotR] - map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX))) : val);
	int error = map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) - SensorValue[armPotR];
	int lspeed = fixDeadZone(val - ARM_KP * error);
	int rspeed = fixDeadZone(val + ARM_KP * error);
	//int lspeed = val;
	//int rspeed = val;
	if ((SensorValue[limitL] != 0 && val < 0) || (SensorValue[armPotL] >= ARM_L_MAX && val > 0))
		lspeed = 0;
	 if ((SensorValue[limitR] != 0 && val < 0) || (SensorValue[armPotR] >= ARM_R_MAX && val > 0))
		rspeed = 0;

	motor[armLBack] = lspeed;
	motor[armRBack] = rspeed;
	motor[armRFront] = rspeed;
	motor[armLFront] = lspeed;
}

void arm(int l, int r)
{
  int lspeed = fixDeadZone(l);
  int rspeed = fixDeadZone(r);
  if ((SensorValue[limitL] != 0 && l < 0)) //|| (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) >= ARM_TOP && l > 0))
		lspeed = 0;
  if ((SensorValue[limitR] != 0 && r < 0)) //|| (SensorValue[armPotR] >= ARM_TOP && r > 0))
		rspeed = 0;

	motor[armLBack] = lspeed;
	motor[armRBack] = rspeed;
	motor[armRFront] = rspeed;
	motor[armLFront] = lspeed;
}

void move(int l, int r)
{
	int newl = fixDeadZone(l);
	int newr = fixDeadZone(r);
	motor[rightFrontMotor] = newr;
	motor[rightBackMotor] = newr;
	motor[leftFrontMotor] = newl;
	motor[leftBackMotor] = newl;
}

void moveRamped(int l, int r)
{
	int newl = fixDeadZone(l);
	int newr = fixDeadZone(r);
	setMotor(rightFrontMotor, newr);
	setMotor(rightBackMotor, newr);
	setMotor(leftFrontMotor, newl);
	setMotor(leftBackMotor, newl);
}

void move(int lf, int lb,int rf,int rb)
{
	motor[rightFrontMotor] = fixDeadZone(rf);
	motor[rightBackMotor]= fixDeadZone(rb);
	motor[leftFrontMotor]= fixDeadZone(lf);
	motor[leftBackMotor]= fixDeadZone(lb);
}

void moveRamped(int lf, int lb, int rf, int rb)
{
	int newlf = fixDeadZone(lf);
	int newlb = fixDeadZone(lb);
	int newrf = fixDeadZone(rf);
	int newrb = fixDeadZone(rb);
	setMotor(rightFrontMotor, newrf);
	setMotor(rightBackMotor, newrb);
	setMotor(leftFrontMotor, newlf);
	setMotor(leftBackMotor, newlb);
}

void intake(int val){
	motor[intakeR]=val;
	motor[intakeL]=val;
}


/**************Pneumatics********************/
void piston1(){
	SensorValue[Solenoid1]= (SensorValue[Solenoid1] == 0)? 1 : 0;
}

void piston2(){
	SensorValue[Solenoid1]= (SensorValue[Solenoid2] == 0)? 1 : 0;
}

void throw() {
	SensorValue[Solenoid1]= 1;
	SensorValue[Solenoid2]= 1;

	wait1Msec(500);

	SensorValue[Solenoid1]= 0;
	SensorValue[Solenoid2]= 0;
}

/********************Auton Tasks***************************/
task driveTask() {
	goForward(driveTaskDistance);
}

task lineTraceTask() {
	lineTrace(driveTaskDistance);
}

task runLiftArm()
{
	//StopTask(runLowerArm);
	if(valLift != -1){
		liftArm(127 * sign(valLift - SensorValue[armPotR]), valLift);
	} else {
		lowerArm(-127);
	}
	valLift = 0;
}

/*task runLowerArm()
{
	StopTask(runLiftArm);
	valLift = -127;
	lowerArm(valLift);
	valLift = 0;
} */

task killSwitch()
{
	StopTask(driveTask);
	StopTask(lineTraceTask);
	StopTask(runLiftArm);
//	StopTask(runLowerArm);
	move(0, 0);
	arm(0);
	intake(0);
}

/*********************Auton Functions***********************/
void waitUntilLine(int val)
{
	while (SensorValue[lEye] > val || SensorValue[mEye] > val || SensorValue[rEye] > val)
		wait1Msec(10);
}

void waitUntilAllLines(int val)
{
	while (SensorValue[lEye] > val && SensorValue[mEye] > val && SensorValue[rEye] > val)
		wait1Msec(10);
}

void moveUntilRamped(int l, int r)
{
	while (motor[rightBackMotor] != fixDeadZone(r) &&	motor[rightFrontMotor] != fixDeadZone(r) && motor[leftFrontMotor] != fixDeadZone(l) && motor[leftBackMotor] != fixDeadZone(l)) {
		moveRamped(l, r);
	}
}

void goForward(int inches)
{
	int deg;
	float encperinch;
	encperinch = 360/(dia*PI*gearratio);
	deg = encperinch * ((float)inches / 1.5);

	clearDriveEncoders();
	while(abs(SensorValue[quadR]) <  abs(deg)) // later add left encoder
	{
		moveRamped(sign(inches) * 127, sign(inches) * 127);
		wait1Msec(10);
	}
	move(0,0);
	clearDriveEncoders();
}

void goBackward(int inches) {
	goForward(-inches / 0.9);
}

void goAsync(int inches) {
	driveTaskDistance = inches;
	StartTask(driveTask);
}

void hang()
{
	liftArm(127, ARM_HANG);
	goBackward(4); //find value
	liftArm(-127, ARM_BAR);
}

void jerk()
{
	move(127, 127);
	wait1Msec(100);
	move(-127, -127);
	wait1Msec(100);
	move(0, 0);
}

/*void lineTrace(int inches) {
	int l, r, m;

	clearDriveEncoders();

	bool onLine = false;
	long onLineTicks = 0;
	int turning = 0;
	while(abs(SensorValue[quadR]) < abs(360/(dia*PI*gearratio)*inches)){
		l = mapLineFollower(SensorValue[lEye]);
		r = mapLineFollower(SensorValue[rEye]);
		m = mapLineFollower(SensorValue[mEye]);

		if (l < 20 && r < 20 && m < 20)
			move(sign(inches)*40, sign(inches)*30);
		else {
			if (onLine)
				move(sign(inches)*(r*3+m)*0.3,sign(inches)*(l*3+m)*0.3);//move(((r*3+m)*0.5 < 35) ? 35 : (r*3+m)*0.5,((l*3+m)*0.5 < 35) ? 35 : (l*3+m)*0.5);
			else {
				if (turning != 0) {
					if (turning == 1)
						move(sign(inches)*50, sign(inches)*25);
					else if (turning == -1)
						move(sign(inches)*30, sign(inches)*40);
					if (m > 50 && (turning == 1 ? l : r) < 30) onLine = true;
				}
				else {
					if (m > 50)
						onLine = true;
					else if (l > 20)
						turning = 1;
					else if (r > 20)
						turning = -1;
				}
			}
		}
	}
	move(0, 0);
}*/

void lineTrace(int inches){
	int l = mapLineFollower(SensorValue[lEye]);
	int r = mapLineFollower(SensorValue[rEye]);
	int m = mapLineFollower(SensorValue[mEye]);
	while(abs(SensorValue[quadR]) < abs(360/(dia*PI*gearratio)*inches)){
		l = mapLineFollower(SensorValue[lEye]);
		r = mapLineFollower(SensorValue[rEye]);
		m = mapLineFollower(SensorValue[mEye]);
		moveRamped(127-l, 127-r);
	}
}

void lineTraceAsync(int inches)
{
	driveTaskDistance = inches;
	StartTask(lineTraceTask);
}

void liftArm(int speed, int armPotValue)
{
	while (((map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) < armPotValue || SensorValue[armPotR] < armPotValue) && speed > 0) || ((map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) > armPotValue || SensorValue[armPotR] > armPotValue) && speed < 0))  {
	  if ((map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) < armPotValue && SensorValue[armPotR] < armPotValue && speed > 0) || (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) > armPotValue && SensorValue[armPotR] > armPotValue && speed < 0))
		arm(speed);
	  else if ((map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) < armPotValue && SensorValue[armPotR] >= armPotValue && speed > 0) || (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) > armPotValue && SensorValue[armPotR] <= armPotValue && speed < 0))
	  	arm(speed / 2, 0);
	  else if ((map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) >= armPotValue && SensorValue[armPotR] < armPotValue && speed > 0) || (map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) <= armPotValue && SensorValue[armPotR] > armPotValue && speed < 0))
	  	arm(0, speed / 2);
	}
	/*arm(speed);
	wait1Msec(armPotValue);
	arm(0);*/
}

void lowerArm(int speed)
{
	while (SensorValue[limitL] == 0 && SensorValue[limitR] == 0)
		arm(speed);
	arm(0);
}

void rturn(int bodydegs) // 1 body deg = wheelbase divided by diameter times gear ratio)
{
	clearDriveEncoders();
	float turndeg;
	float encdegperbodydeg = wheelbase / (dia * gearratio);
	turndeg = encdegperbodydeg * bodydegs;

	while(abs(SensorValue(quadR)) < abs(turndeg))
	{
		moveRamped(100, -100);
		wait1Msec(10);
	}

	move(0,0);
	clearDriveEncoders();
}

void lturn(int bodydegs) // 1 body deg = wheelbase divided by diameter times gear ratio)
{
	clearDriveEncoders();
	float turndeg;
	float bodydegsperencdeg = wheelbase / (dia * gearratio);
	turndeg = bodydegsperencdeg * bodydegs;

	while(abs(SensorValue(quadR)) < abs(turndeg))
	{
		moveRamped(-100, 100);
		wait1Msec(10);
	}

	move(0,0);
	clearDriveEncoders();
}

void pre_auton()
{
	bLCDBacklight = true;

	autonNames[0] = "easyAuton";
	autonNames[1] = "stashAuton";
	autonNames[2] = "block BLUE";
	autonNames[3] = "block RED";
	autonNames[4] = "hZ SAFE";
	autonNames[5] = "stash_trace_b";
	autonNames[6] = "hZ GET BUCKY";
	autonNames[7] = "hZ OVER BUMP";
	autonNames[8] = "everyT HANG BLUE";
	autonNames[9] = "everyT HANG RED";
	autonNames[10] = "stash_trace_r";
	autonNames[11] = "Suicide (debug)";
	autonNames[12] = "knockover RED";
	autonNames[13] = "knockover BLUE";
	autonNames[14] = "P SkillZ";
	autonNames[15] = "PSkillZ button";
	autonNames[16] = "untie red";
	autonNames[17] = "untie blue";
	autonNames[18] = "timeAuton";
	autonNames[19] = "kasy skills";

	//for(int j = 12; j<14; j++){
		//autonNames[j] = "DO NOTHING";
	//}

	displayLCDCenteredString(0,name);

	while(bIfiRobotDisabled == true)
	{
		if(!locked){
			count = (int) SensorValue[autonPot]/fourBitFactor;
			if(count > 19){count = 19;}
			displayChange(count);
		}

		if(nLCDButtons == centerButton){
			waitForRelease();
			if(locked){
				locked = false;
				clearLCD();
				displayLCDCenteredString(0,name);
				displayLCDCenteredString(1,autonNames[count]);

				}else{
				locked = true;
				clearLCD();
				displayLCDCenteredString(0,autonNames[count]);
				displayLCDCenteredString(1,"is Locked");
			}
		}

		if(nLCDButtons == rightButton){
			batteryLCD();
			waitForRelease();
			displayLCDCenteredString(0,name);
			displayLCDCenteredString(1,autonNames[count]);
		}

	}

	bStopTasksBetweenModes = true; //Keep on true
	clearDriveEncoders();
}


/************** Programming Skills Functions ************/
void kasySkillsTime(){
	hanging_GET_BUCKY_NOSHOOT();
	goBackward(TILE_WIDTH);
	waitAutonButton();
	displayLCDCenteredString(0,"PRESS THE BUTTON");
	liftArm(127, ARM_BAR);
	goForward(DIST_START_BALL90 - 2);
	lturn(90);
	goForward(TILE_WIDTH * 1.5);
	goForward(TILE_WIDTH * 2.5);
	lowerArm(-127);
	rturn(90);
	lineTrace(TILE_WIDTH * 0.5);
	lineTraceAsync(TILE_WIDTH * 1);
	liftArm(127, ARM_STASH);
	wait1Msec(WAIT);
	intake(-40);
	wait1Msec(STASH_TIME);
	intake(127);
	goBackward(TILE_WIDTH*0.5);
	lturn(90+45);
	liftArm(-127, ARM_BAR);
	goForward(sqrt(2)* TILE_WIDTH);
	goForward(3);
	wait1Msec(INTAKE_TIME);
	goBackward(3);
	goBackward(sqrt(2)* TILE_WIDTH);
	rturn(90+45);
	liftArm(127, ARM_STASH);
	goForward(TILE_WIDTH * 0.5);
	wait1Msec(WAIT);
	intake(127);
	wait1Msec(STASH_TIME);
	intake(0);
	goBackward(TILE_WIDTH*0.5);
	lturn(90 + 68);
	lowerArm(-127);
	lturn(90-68);
	goForward(TILE_WIDTH);
	hanging_GET_BUCKY();
	lturn(180);
	goForward(TILE_WIDTH);
	lturn(90 + 45);
	goForward(TILE_WIDTH * sqrt(2) + 5);
	goBackward(TILE_WIDTH * sqrt(2) + 5);
	rturn(45);
	goForward(TILE_WIDTH*2);
	lturn(90);
	goForward(TILE_WIDTH + 5);
	goBackward(TILE_WIDTH + 5);
	rturn(45);
	goForward(TILE_WIDTH * sqrt(2) + 5);
	goBackward(TILE_WIDTH * sqrt(2) + 5);
	rturn(45);
	goForward(TILE_WIDTH*2);
	lturn(90);
	goForward(TILE_WIDTH + 5);
	goBackward(TILE_WIDTH + 5);
	rturn(90);
	goForward(TILE_WIDTH);
	lturn(90 + 45);
	liftArm(127, ARM_HANG);
	wait1Msec(WAIT);
}

void ProgrammingSkills()
{ //fix this completely
	hanging_GET_OVER_BUMP();
	waitAutonButton();
	goForward(DIST_START_BALL90);
	goBackward(DIST_START_BALL90);
	waitAutonButton();
	goForward(DIST_HALFWAY_STASH);

	goAsync(DIST_HALFWAY_STASH);
	liftArm(127, ARM_STASH);
	wait1Msec(WAIT);
	moveUntilRamped(127,0);
	wait1Msec(WAIT);
	move(0,0);
	intake(-50);
	wait1Msec(STASH_TIME);
	goBackward(TILE_WIDTH / 4);

	lowerArm(-127);

	goBackward(TILE_WIDTH*2.2);

	liftArm(127,ARM_STASH);
	lturn(27); // find value later with testing slight lturn
	goAsync(TILE_WIDTH * sqrt(3));
	liftArm(127, ARM_STASH);

	rturn(27);
	goBackward(TILE_WIDTH/2);
	lowerArm(-127);
	rturn(180);

	goForward(TILE_WIDTH*1.2);
	intake(127);
	moveUntilRamped(50,50);
	wait1Msec(BUMP_INTAKE_TIME);
	move(0,0);
	goBackward(TILE_WIDTH/2);
	rturn(90);
	goForward(TILE_WIDTH*3.5);
	rturn(90);

	liftArm(127,ARM_BAR);
	goForward(DIST_START_BALL90);
	goBackward(DIST_START_BALL90);
	rturn(11);
	goForward(DIST_HALFWAY_STASH);

	goAsync(DIST_HALFWAY_STASH);
	liftArm(127, ARM_STASH);
	wait1Msec(WAIT);
	moveUntilRamped(0,127);
	wait1Msec(WAIT);
	move(0,0);
	intake(-50);
	wait1Msec(STASH_TIME);
	goBackward(TILE_WIDTH / 4);

	lowerArm(-127);

	goBackward(TILE_WIDTH*2.2);

	liftArm(127,ARM_BAR * 0.8);
	rturn(27);
	goAsync(TILE_WIDTH * sqrt(3));
	liftArm(127, ARM_BAR * 0.2);
	wait1Msec(WAIT);

	moveUntilRamped(-60,-60);
	waitUntilLine(2700);
	move(0,0);
	rturn(63);
	moveUntilRamped(-60,-60);
	waitUntilLine(2700);
	move(0,0);
	lturn(90);
	goBackward(TILE_WIDTH);
	moveUntilRamped(-60,-60);

	waitUntilAllLines(2700);
	move(0,0);

	goBackward(TILE_WIDTH);

	rturn(45);
	goForward(1.5* sqrt(2)*TILE_WIDTH);
	wait1Msec(WAIT/2);
	goBackward((1.5* sqrt(2)*TILE_WIDTH)-4);
	wait1Msec(WAIT/2);
	hang();
}

void ProgrammingSkills_withButton()
{
	while(button<=2)
	{
		if(button == 0)
		{

			liftArm(127, ARM_BAR);

			goForward(DIST_START_BALL90);
			goBackward(DIST_START_BALL90);
			move(0,0);
			wait1Msec(KASY_QUICK_TURN);
		}

		if(button == 1)
		{
			goForward(DIST_START_BALL45);
			goBackward(DIST_START_BALL45);
			move(0,0);
			wait1Msec(KASY_QUICK_TURN);
		}
		if(button == 2)
		{
			goForward(DIST_START_3RDBALL);
			move(0,0);
			rturn(90);
			goForward(DIST_START_BALL90);
			goBackward(DIST_START_BALL90);
			move(0,0);
			lturn(30);
			goForward(DIST_START_BALL45);
			goBackward(DIST_START_BALL45);

			wait1Msec(KASY_QUICK_TURN);
			button=3;
		}

		if(SensorValue[autonButton] == 1){
			while(SensorValue[autonButton] != 0){}
			button++;
		}
	}

}



/**************************AUTONOMOUS PROCEDURES*****************************/

// control arm height with autonPot
/*void controlArm_Manual()
{
	int mappedValue;
	while (SensorValue[autonButton] == 0) {
		mappedValue = map(SensorValue[autonPot], 0, 4095, ARM_LOW, ARM_TOP);
		if (mappedValue - ((map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) + SensorValue[armPotR]) / 2) > 3)
			arm(-127);
		else if (mappedValue - ((map(SensorValue[armPotL], ARM_L_MIN, ARM_L_MAX, ARM_R_MIN, ARM_R_MAX) + SensorValue[armPotR]) / 2) < -3)
		    arm(127);
		else
			arm(0);
	}
}*/

void easyBack_Front()
{
	intake(127);
	goForward(TILE_WIDTH * 1.5);
	liftArm(127,ARM_BAR);
	lturn(90);
	goBackward(TILE_WIDTH*.5);
	goForward(TILE_WIDTH*3.2);
	intake(-127);
	goBackward(TILE_WIDTH*1.5);
}

void easyBack_noFront()
{
	easyBack_Front();
	lturn(45);
	goForward(TILE_WIDTH*2.12);
}


//mess up black tie's (2616B) auton
void untie_RED()
{
	goForward(TILE_WIDTH*3.6);
	lturn(90);
	lineTrace(DIST_HALFWAY_STASH);
	lineTraceAsync(DIST_HALFWAY_STASH);
	//liftArm(127,ARM_STASH);
	liftArm(127,ARM_STASH);
	wait1Msec(WAIT/3);
	intake(-127);
	wait1Msec(STASH_TIME);
	rturn(25);
	goBackward(TILE_WIDTH*5);
	waitAutonButton();
	easyAuton();

}

void untie_BLUE()
{
	goForward(TILE_WIDTH*3.6);
	rturn(90);
	lineTrace(DIST_HALFWAY_STASH);
	lineTraceAsync(DIST_HALFWAY_STASH);
	//liftArm(127,ARM_STASH);
	liftArm(127,ARM_STASH);
	wait1Msec(WAIT/3);
	intake(-127);
	wait1Msec(STASH_TIME);
	lturn(25);
	goBackward(TILE_WIDTH*5);
	waitAutonButton();
	easyAuton();
}

//Goes to opposing teams area and attempts to block autonomous
void block_BLUE()//blocks the team in the blue zone
{
	goForward(2.5 * TILE_WIDTH);
	lturn(90);
	goForward(DIST_START_3RDBALL + 30);
}

void block_RED()// blocks the team in the red zone
{
	goForward(2.5 * TILE_WIDTH);
	rturn(90);
	goForward(DIST_START_3RDBALL + 30);
}

// Hits the 2 big balls on the 12 inch bar on Blue side
void easyAuton()
{
	//liftArm(127, ARM_BAR - 1000);
	liftArm(127, ARM_BAR - 1000);
	goAsync(DIST_START_BALL90);
	//liftArm(127, 1000);
	liftArm(127, 1000);
	wait1Msec(WAIT / 2);
	goBackward(DIST_START_BALL90);

	waitAutonButton();

	goForward(DIST_START_BALL45);
	intake(127);
	wait1Msec(WAIT / 2);
	goBackward(DIST_START_BALL45);
}

void timeAuton(){

	//liftArm(127, ARM_BAR - 1000);
	liftArm(127, ARM_BAR - 1000);
	moveUntilRamped(127,127);
	//liftArm(127, 1000);
	liftArm(127, 1000);
	wait1Msec(2000);
	moveUntilRamped(-127,-127);
	wait1Msec(2000);

	waitAutonButton();

	moveUntilRamped(127,127);
	wait1Msec(3000);
	moveUntilRamped(-127,-127);
	wait1Msec(3000);

}


//Stashes 1 bucky ball and then performs easyAuton()
void stashAuton()
{
	goForward(DIST_HALFWAY_STASH + 10);

	goAsync(DIST_HALFWAY_STASH - 5);
	//liftArm(127, ARM_STASH);
	liftArm(127, ARM_STASH);

	intake(40);
	wait1Msec(STASH_TIME);
	/* goBackward(TILE_WIDTH / 8);

	lowerArm(-127);
	goBackward(DIST_STASH); */
	goBackward(TILE_WIDTH / 8);
	lowerArm(-127);
	goForward(-DIST_HALFWAY_STASH + 5);
	goForward(-DIST_HALFWAY_STASH - 10);
	waitAutonButton();
	easyAuton();
}

void stashAuton_lineTracer_RED()// stashing with line tracer
{
	//liftArm(127, ARM_BAR - 1000);
	liftArm(127, ARM_BAR - 1000);
	goAsync(DIST_START_BALL45);
	liftArm(127, 1000);
	goBackward(DIST_START_BALL45);
	lowerArm(-127);
	goForward(TILE_WIDTH * sqrt(2));
	lturn(45);
	goForward(1.5 * TILE_WIDTH);
	//liftArm(127, ARM_STASH - 1000);
	liftArm(127 , ARM_STASH - 1000);
	goAsync(TILE_WIDTH);
	liftArm(127, 1000);
	intake(-70);
	wait1Msec(INTAKE_TIME);


	/*goForward(DIST_STASH * 3 / 4);
	liftArm(127, ARM_STASH);
	move(50, 50);
	waitUntilLine(LINE_VALUE);
	rturn(5);

	lineTrace(TILE_WIDTH * 3 / 2);
	intake(127);
	wait1Msec(STASH_TIME);
	intake(0);

	goBackward(10);
	lowerArm(-127);
	lturn(11);
	goBackward(68);
	easyAuton();*/
}

void stashAuton_lineTracer_BLUE()// stashing with line tracer
{
	//liftArm(127, ARM_BAR - 1000);
	liftArm(127, ARM_BAR - 1000);
	goAsync(DIST_START_BALL45);
	liftArm(127, 1000);
	goBackward(DIST_START_BALL45);
	lowerArm(-127);
	goForward(TILE_WIDTH * sqrt(2));
	lturn(45);
	goForward(1.5 * TILE_WIDTH);
	//liftArm(127, ARM_STASH - 1000);
	liftArm(127 , ARM_STASH - 1000);
	goAsync(TILE_WIDTH);
	liftArm(127, 1000);
	intake(-70);
	wait1Msec(INTAKE_TIME);
}

void hanging_SAFE()
{
	goForward(DIST_START_HANGING_RAND_BALLS);
	wait1Msec(WAIT);
	goBackward(DIST_START_HANGING_RAND_BALLS);
	waitAutonButton();
	intake(-70);
}

void hanging_GET_BUCKY()// get buckies from back tile
{
	wait1Msec(WAIT);
	intake(127);
	goForward(TILE_WIDTH);
	wait1Msec(INTAKE_TIME);
	//liftArm(127, 500);
	goBackward(TILE_WIDTH);
	rturn(180);
	intake(127);
	wait1Msec(5000);
	intake(0);
}

void hanging_GET_BUCKY_NOSHOOT()// get buckies from back tile
{
	wait1Msec(WAIT);
	intake(127);
	goForward(TILE_WIDTH);
	wait1Msec(INTAKE_TIME);
//	intake(0);
	//liftArm(127, 500);
	goBackward(TILE_WIDTH);
}

void suicide(){
	while(true){
		liftArm(127, ARM_BAR);
		goForward(10);
		goBackward(10);
		lturn(45);
		goBackward(10);
		rturn(45);
		lturn(720);
		goForward(10);
		rturn(720);
		goBackward(10);
		liftArm(-127, ARM_BAR);
		goForward(10);
		goBackward(10);
		lturn(45);
		goBackward(10);
		rturn(45);
		lturn(720);
		goForward(10);
		rturn(720);
		goBackward(10);
		for(int x=0;x<30;x++){
			move(127,127);
			move(-127,-127);
		}
	}
}

void hanging_GET_OVER_BUMP()
{
	hanging_GET_BUCKY_NOSHOOT();
	liftArm(127, 800);
	goBackward(2 * TILE_WIDTH);
}

void knockover_BALLS_RED()
{
	hanging_GET_BUCKY();
	goBackward(TILE_WIDTH/4);
	lturn(135);
	//liftArm(127);
	goForward(1.5* sqrt(2)*TILE_WIDTH);
	move(0,0);
}

void knockover_BALLS_BLUE()
{
	hanging_GET_BUCKY();
	goBackward(TILE_WIDTH/4);
	rturn(135);
	//liftArm(127);
	goForward(1.5* sqrt(2)*TILE_WIDTH);
	move(0,0);
}

void hang_RED()
{
	lineTrace(-TILE_WIDTH * 2);
	goForward(TILE_WIDTH / 2);
	rturn(45);
	goForward(3);
	hang();
}

void hang_BLUE()
{
	lineTrace(-TILE_WIDTH * 2);
	goForward(TILE_WIDTH / 2);
	lturn(45);
	goForward(3);
	hang();
}

void everything_HANGING_BLUE_()
{
	knockover_BALLS_BLUE();
	rturn(45);
	liftArm(127, ARM_BAR);
	goForward(DIST_START_BALL90);
	goBackward(DIST_START_BALL90);
	rturn(45);
	goForward(DIST_START_BALL45);
}

void everything_HANGING_RED_()
{

	knockover_BALLS_RED();
	lturn(45);
	liftArm(127, ARM_BAR);
	goForward(DIST_START_BALL90);
	goBackward(DIST_START_BALL90);
	lturn(45);
	goForward(DIST_START_BALL45);
}

task autonomous()
{
	//0(0000) - easyAuton();
	//1(0001) - stashAuton();
	//2(0010) - block_BLUE();
	//3(0011) - block_RED();
	//4(0100) - hanging_SAFE_BLUE();
	//5(0101) - hanging_SAFE_RED();
	//6(0110) - hanging_GET_BUCKY();
	//7(0111) - hanging_GET_OVER_BUMP();
	//8(1000) - nothing
	//9(1001) - nothing
	//10(1010) - nothing
	//11(1011) - nothing
	//12(1100) - nothing
	//13(1101) - nothing
	//14(1110) - nothing
	//15(1111) - nothing

	displayChange(-1);
	clearDriveEncoders();

	intake(-127);
	wait1Msec(2000);
	intake(0);
	wait1Msec(1000);

	switch(count)
	{
	case(0): easyAuton();
		break;
	case(1): stashAuton();
		break;
	case(2): block_BLUE();
		break;
	case(3): block_RED();
		break;
	case(4): hanging_SAFE();
		break;
	case(5): stashAuton_lineTracer_BLUE();
		break;
	case(6): hanging_GET_BUCKY();
		break;
	case(7): hanging_GET_OVER_BUMP();
		break;
	case(8): everything_HANGING_BLUE_();
		break;
	case(9): everything_HANGING_RED_();
		break;
	case(10): stashAuton_lineTracer_RED();
		break;
	case(11): suicide();
		break;
	case(12): knockover_BALLS_RED();
		break;
	case(13): knockover_BALLS_BLUE();
		break;
	case(14): ProgrammingSkills();
		break;
	case(15): ProgrammingSkills_withButton();
		break;
	case(16) : untie_BLUE();
		break;
	case(17) : untie_RED();
		break;
	case(18): timeAuton();
		break;
	case(19): kasySkillsTime();
	  break;
	}


}

task usercontrol()
{
	ClearTimer(T2);
	bLCDBacklight = true;
	//drawName(0);
	//messageLCD();
	clearDriveEncoders();
	clearMotorArrayValues();
	//clearArmEncoders();
	//*****************CHASSIS MOVEMENT************************/

	while (true)
	{
		/* LCD Code */
		if(nLCDButtons == rightButton){
			batteryLCD();
			waitForRelease();
			bLCDBacklight = true;
			clearLCD();
			drawName(0);
			messageLCD();
		}

		if (time1[T1] > 3000) {
			ClearTimer(T1);
			clearLCDLine(1);
			messageLCD();
		}
		/* Motion Code */
		moveRamped(vexRT[Ch3] + vexRT[Ch1], vexRT[Ch3] + vexRT[Ch1], vexRT[Ch3] - vexRT[Ch1], vexRT[Ch3] - vexRT[Ch1]);

		// arm
		if (vexRT[Btn5U] == 1)
			arm(127);
		else if (vexRT[Btn5D] == 1)
			arm(-127);
		else if (valLift == 0)
			arm(0, 0);

		if (vexRT[Btn5U] == 1)
			arm(127);
	  else if (vexRT[Btn5D] == 1)
			arm(-127);
		else if (vexRT[Btn7LXmtr2] == 1 /*|| vexRT[Btn7L] == 1*/)
			arm(127, 0);
	  else if (vexRT[Btn7RXmtr2] == 1 /*|| vexRT[Btn7R] == 1*/)
	  	arm(0, 127);
		else if (vexRT[Btn8LXmtr2] == 1 /*|| vexRT[Btn8L] == 1*/)
			arm(-127, 0);
	  else if (vexRT[Btn8RXmtr2] == 1 /*|| vexRT[Btn8R] == 1*/)
	  	arm(0, -127);

//	  if (vexRT[Btn8U] == 1){ intake(-127); wait1Msec(1000); intake(0); stashAuton(); }
		// pneumatics code
		/*if(vexRT[Btn8D] == 1)
			throw();
		if(vexRT[Btn8L] == 1)
			piston1();
		if(vexRT[Btn8R] == 1)
			piston2();*/

		if (vexRT[Btn7U] == 1){
	  	valLift = ARM_STASH;
	  	StartTask(runLiftArm);
	  }

	  if(vexRT[Btn7L] == 1){
	  	valLift = ARM_HANG;
	  	StartTask(runLiftArm);
	  }
		if(vexRT[Btn7R] == 1){
			valLift = ARM_BAR;
			StartTask(runLiftArm);
		}
	  if(vexRT[Btn7D] == 1){
	  	valLift = -1;
	  	StartTask(runLiftArm);
	  }

	  if(vexRT[Btn8UXmtr2] == 1)
	  	StartTask(killSwitch);
		if(vexRT[Btn8U] == 1){
			StartTask(killSwitch);
		}
		// intake code

	  if (fixDeadZone(vexRT[Ch3Xmtr2]) != 0)
	  	intake(vexRT[Ch3Xmtr2]);
		if (vexRT[Btn6U] == 1)
			intake(127);
		else if (vexRT[Btn6D] == 1)
			intake(-127);
		else if (vexRT[Btn6UXmtr2] == 1)
			intake(127);
		else if (vexRT[Btn6DXmtr2] == 1)
			intake(-127);
		else if ((vexRT[Btn6D] == 0) && (vexRT[Btn6U] == 0) && (fixDeadZone(vexRT[Ch3Xmtr2]) == 0) && vexRT[Btn6UXmtr2] == 0 && vexRT[Btn6DXmtr2] == 0 )
			intake(0);
	}
}
