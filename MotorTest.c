#pragma config(Motor,  port1,           leftBackMotor, tmotorVex393, openLoop)
#pragma config(Motor,  port2,           intakeL,       tmotorVex393, openLoop)
#pragma config(Motor,  port3,           intakeR,       tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port4,           leftFrontMotor, tmotorVex393, openLoop)
#pragma config(Motor,  port5,           rightFrontMotor, tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port6,           armLBack,      tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port7,           armRFront,     tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port8,           armRBack,      tmotorVex393, openLoop)
#pragma config(Motor,  port9,           armLFront,     tmotorVex393, openLoop)
#pragma config(Motor,  port10,          rightBackMotor, tmotorVex393, openLoop, reversed)

#define speed 127
task main()
{
while(true){
	motor[port1] = speed;
	motor[port2] = speed;
	motor[port3] = speed;
	motor[port4] = speed;
	motor[port5] = speed;
	motor[port6] = speed;
	motor[port7] = speed;
	motor[port8] = speed;
	motor[port9] = speed;
	motor[port10] = speed;
	wait1Msec(2000);
	motor[port1] = -speed;
	motor[port2] = -speed;
	motor[port3] = -speed;
	motor[port4] = -speed;
	motor[port5] = -speed;
	motor[port6] = -speed;
	motor[port7] = -speed;
	motor[port8] = -speed;
	motor[port9] = -speed;
	motor[port10] = -speed;
	wait1Msec(2000);
}


}
